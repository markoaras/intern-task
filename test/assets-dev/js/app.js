var testApp = angular.module("app", []);

testApp.controller("featuresController",["$scope", "featuresService" , function ($scope, featuresService) {
    $scope.features = featuresService.getFeatures();
}]);

testApp.controller("footerController",["$scope", "socialMediaService" , function ($scope, socialMediaService) {
    $scope.socialMedias = socialMediaService.getSocialMedias();
}]);




