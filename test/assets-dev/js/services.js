testApp.factory('featuresService', [function() {

    return {
        getFeatures : function(){
            var features = [
                {
                    "img": "pics/Features_icons/feature-one-icon.png",
                    "title": "Ut enim ad minim",
                    "description": "Quis nostrud exercitation",
                    "text": "Cumque reiciendis, asperiores iste fuga nihil velit deleniti perspiciatis corporis et aspernatur repudiandae at sequi doloribus rem consectetur, expedita quae quos dolor dicta qui molestiae est saepe pariatur, tenetur unde!"
                },

                {
                    "img": "pics/Features_icons/feature-two-icon.png",
                    "title": "Molit anim id est",
                    "description": "Exceptur sint occaecat",
                    "text": "Eius repudiandae qui recusandae officiis sit totam voluptatibus eligendi fugiat nostrum cupiditate. In nulla nihil dolore aut fuga sint expedita ducimus quibusdam ipsam iure dolor repellendus eius, totam! A, illo. Nihil cupitate nobis aut!"
                },

                {
                    "img": "pics/Features_icons/feature-three-icon.png",
                    "title": "Sunt in culpa qui",
                    "description": "Deserunt molit anim id",
                    "text": "Debitis alias beatae, optio soluta suscipit unde vel, id, fugiat modi quibusdam explicabo possimus et sapiente vitae perspiciatis dicta, nihil libero. Eveniet quisquam numquam incidunt, inventore, aperiam provident ratione est."
                }
            ];
            return features;
        }
    }
}]);

testApp.factory('socialMediaService', [function() {

    return {
        getSocialMedias : function(){
            var socialMedias = [
                {
                    "title": "facebook",
                    "img": "pics/footer_icons/social-facebook.png",
                    "imgRetina": "pics/footer_icons/social-facebook@2x.png",
                    "url": "https://www.facebook.com/arsfutura.co/?fref=ts"
                },
                {
                    "title": "twitter",
                    "img": "pics/footer_icons/social-twitter.png",
                    "imgRetina": "pics/footer_icons/social-twitter@2x.png",
                    "url": " https://twitter.com/arsfutura_co"

                },
                {
                    "title": "instagram",
                    "img": "pics/footer_icons/social-instagram.png",
                    "imgRetina": "pics/footer_icons/social-instagram@2x.png",
                    "url": " https://instagram.com/arsfutura/"
                },
                {
                    "title": "googlePlus",
                    "img": "pics/footer_icons/social-google-plus.png",
                    "imgRetina": "pics/footer_icons/social-google-plus@2x.png",
                    "url": "https://plus.google.com/+arsfuturaco/about"
                },
                {
                    "title": "youtube",
                    "img": "pics/footer_icons/social-youtube.png",
                    "imgRetina": "pics/footer_icons/social-youtube@2x.png",
                    "url": "https://plus.google.com/+arsfuturaco/about"
                }
            ];
            return socialMedias;
        }
    }
}])