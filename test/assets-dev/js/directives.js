testApp.directive('customHeader', [function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'assets/html_templates/directives/custom-header.html',
        link: function (scope) {

            scope.goToElement = function (elementId) {
                $('html, body').animate({
                    scrollTop: $("#"+elementId).offset().top
                }, 1000);
            }
        }
    };
}]);