module.exports = function (grunt) {

    require('jit-grunt')(grunt);

    grunt.initConfig({
        less: {
            dev: {
                options: {
                    paths: []
                },
                files: {
                    "assets/css/main.css": "assets-dev/less/main.less"
                }
            }
        },

        concat: {
            dev: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/angular/angular.js',
                    'assets-dev/js/app.js',
                    'assets-dev/js/directives.js',
                    'assets-dev/js/services.js'
                ],
                dest: 'assets/js/build.js'
            }
        },

        minified: {
            files: {
                src: [
                    //'assets/js/**/*.js',
                    //'assets/js/*.js'
                ],
                dest: [
                    'assets/js/'
                ]
            },
            options: {
                sourcemap: false,
                allinone: false
            }
        },

        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'assets/css',
                    src: ['*.css'],
                    dest: 'assets/css',
                    ext: '.css'
                }]
            }
        },

        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'assets/css/*.css',
                        '*.html'
                    ]
                },
                options: {
                    proxy: null
                }
            }
        },

        watch: {
            styles: {
                files: ['assets-dev/less/**/*.less', 'assets-dev/js/**/*.js'],
                tasks: ['less', 'cssmin', 'concat', 'minified'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask('default', ['less', 'cssmin', 'concat', 'minified', 'watch']);
    grunt.registerTask('startServer', ['browserSync']);
    grunt.registerTask('less: compile and minify', ['less', 'cssmin']);
    grunt.registerTask('less: compile ', ['less']);
};